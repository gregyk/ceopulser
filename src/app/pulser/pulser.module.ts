import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PulserPageRoutingModule } from './pulser-routing.module';

import { PulserPage } from './pulser.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PulserPageRoutingModule
  ],
  declarations: [PulserPage]
})
export class PulserPageModule {}
