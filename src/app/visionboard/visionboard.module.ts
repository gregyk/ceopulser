import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { VisionboardPageRoutingModule } from './visionboard-routing.module';

import { VisionboardPage } from './visionboard.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    VisionboardPageRoutingModule
  ],
  declarations: [VisionboardPage]
})
export class VisionboardPageModule {}
