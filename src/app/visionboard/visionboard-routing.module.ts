import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { VisionboardPage } from './visionboard.page';

const routes: Routes = [
  {
    path: '',
    component: VisionboardPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class VisionboardPageRoutingModule {}
