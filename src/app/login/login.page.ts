import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
    loginData = {
    email: '',
    password: ''
  };

  constructor(
      public toastController: ToastController,
  public afAuth: AngularFireAuth,
  private router: Router 
      ) { }

  ngOnInit() {
  }
  
  login() {
      this.afAuth.signInWithEmailAndPassword(this.loginData.email, this.loginData.password)
      .then(auth => {
          this.router.navigateByUrl('/');
      }) .catch(err => {
          this.errorMail();
      });
    }
    
    signUp() {
      this.afAuth.createUserWithEmailAndPassword(this.loginData.email, this.loginData.password)
        .then(auth => {
          console.log('ID de l utilisateur: ' + auth.user.uid); 
          this.router.navigateByUrl('/signup'); 
        }) .catch(err => {
          console.log('Erreur: ' + err);
          this.errorSignup();
        });
    }
    
    async errorMail() {
      const toast = await this.toastController.create({
        message: 'Email ou mot de passe incorrect',
        duration: 2000,
        position: 'top'
      });
      toast.present();
    }
    
    async errorSignup() {
      const toast = await this.toastController.create({
        message: 'Email incorrect ou mot de passe trop court',
        duration: 2000,
        position: 'top'
      });
      toast.present();
    }

}
