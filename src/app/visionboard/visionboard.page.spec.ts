import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { VisionboardPage } from './visionboard.page';

describe('VisionboardPage', () => {
  let component: VisionboardPage;
  let fixture: ComponentFixture<VisionboardPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VisionboardPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(VisionboardPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
