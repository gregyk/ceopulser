import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PulserPage } from './pulser.page';

const routes: Routes = [
  {
    path: '',
    component: PulserPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PulserPageRoutingModule {}
